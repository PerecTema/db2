/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS;

import MODELS.Entity.Obrazky;
import MODELS.GRUD.Delete;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Artem
 */
public class ImageClass {

    public static ImageView createImageView(final Obrazky imageFile, Stage stage) {
        // DEFAULT_THUMBNAIL_WIDTH is a constant you need to define
        // The last two arguments are: preserveRatio, and use smooth (slower)
        // resizing

        ImageView imageView = null;
        final Image image = new Image(new ByteArrayInputStream(imageFile.getObrazek()), 150, 150, true, true);
        imageView = new ImageView(image);
        imageView.setFitWidth(150);
        imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent mouseEvent) {

                if (mouseEvent.getButton().equals(MouseButton.PRIMARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        BorderPane borderPane = new BorderPane();
                        ImageView imageView = new ImageView();
                        Image image = new Image(new ByteArrayInputStream(imageFile.getObrazek()));
                        imageView.setImage(image);
                        imageView.setStyle("-fx-background-color: BLACK");
                        imageView.setFitHeight(stage.getHeight() - 100);
                        imageView.setPreserveRatio(true);
                        imageView.setSmooth(true);
                        imageView.setCache(false);
                        borderPane.setCenter(imageView);
                        borderPane.setStyle("-fx-background-color: BLACK");
                        Stage newStage = new Stage();
                        newStage.setWidth(stage.getWidth());
                        newStage.setHeight(stage.getHeight());
                        Scene scene = new Scene(borderPane, Color.BLACK);
                        newStage.setScene(scene);
                        newStage.show();

                    }
                }
                if (mouseEvent.getButton().equals(MouseButton.SECONDARY)) {
                    if (mouseEvent.getClickCount() == 2) {
                        try {
                            Delete.obrazekDelete(imageFile);
                        } catch (SQLException ex) {
                            System.out.println(ex.getMessage());
                        }
                    }
                }
            }
        });
        return imageView;
    }
}
