/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class MistoVydani {

    public static final String TABLE = "SEM_MISTA_VYDANI";
    public static final String COL_ID = "ID_MISTO_VYDANI";
    public static final String COL_JMENO = "NAZEV_MISTA";

    private Integer id;
    private String nazevMista;

    public MistoVydani(Integer id, String nazevMista) {
        this.id = id;
        this.nazevMista = nazevMista;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNazevMista() {
        return nazevMista;
    }

    public void setNazevMista(String nazevMista) {
        this.nazevMista = nazevMista;
    }

    @Override
    public String toString() {
        return nazevMista;
    }
}
