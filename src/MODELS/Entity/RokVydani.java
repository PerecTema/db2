/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class RokVydani {
    
    
    public static final String TABLE = "SEM_ROK_VYDANI";
    public static final String COL_ID = "ID_ROKU";
    public static final String COL_ROK_ZACATEK = "ROK_ZACATEK";
    public static final String COL_ROK_KONEC = "ROK_KONEC";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";
    
    private Integer idRoku;
    private Integer rokZacatek;
    private Integer rokKonec;

    public RokVydani(Integer idRoku, Integer rokZacatek, Integer rokKonec) {
        this.idRoku = idRoku;
        this.rokZacatek = rokZacatek;
        this.rokKonec = rokKonec;
    }


    public Integer getId() {
        return idRoku;
    }

    public void setIdRoku(Integer idRoku) {
        this.idRoku = idRoku;
    }

    public Integer getRokZacatek() {
        return rokZacatek;
    }

    public void setRokZacatek(Integer rokZacatek) {
        this.rokZacatek = rokZacatek;
    }

    public Integer getRokKonec() {
        return rokKonec;
    }

    public void setRokKonec(Integer rokKonec) {
        this.rokKonec = rokKonec;
    }    
    
    
}
