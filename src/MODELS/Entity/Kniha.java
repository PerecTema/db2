/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

import MODELS.GRUD.Select;

/**
 *
 * @author Sarlota
 */
public final class Kniha {

    public static final String TABLE = "SEM_KNIHA";
    public static final String COL_ID = "ID_KNIHY";
    public static final String COL_EAN = "CAROVY_KOD";
    public static final String COL_SIGNATURA = "SIGNATURA";
    public static final String COL_AUTOR_ID = "AUTOR_ID";
    public static final String COL_TISKAR_ID = "TISKAR_ID";
    public static final String COL_KATEGORIE_ID = "KATEGORIE_ID";
    public static final String COL_MISTO_VYDANI_ID = "MISTO_VYDANI_ID";
    public static final String COL_NAZEV = "NAZEV_KNIHY";
    public static final String COL_POPIS = "POPIS_KNIHY";
    public static final String COL_ROK_VYDANI = "ROK_VYDANI_ID";
    public static final String COL_TITUL_STR_ID = "TITUL_STR_ID";
    public static final String COL_PRVNI_STR_ID = "PRVNI_STR_ID";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";

    private Integer idKnihy;
    private String carovyKod;
    private String nazev;
    private String signatura;
    private String popisKnihy;
    private Autor autorId;
    private Kategorie kategorieId;
    private Tiskar tiskarId;
    private MistoVydani mistoVydaniId;
    private Obrazky titulniStranka;
    private Obrazky prvniStranka;
    private RokVydani rokVydani;

    public Kniha(Integer idKnihy, String carovyKod, String nazev, String signatura, String popisKnihy, Autor autorId, Kategorie kategorieId, Tiskar tiskarId, MistoVydani mistoVydaniId, Obrazky titulniStranka, Obrazky prvniStranka, RokVydani rokVydani) {
        this.idKnihy = idKnihy;
        this.carovyKod = carovyKod;
        this.nazev = nazev;
        this.signatura = signatura;
        this.popisKnihy = popisKnihy;
        this.autorId = autorId;
        this.kategorieId = kategorieId;
        this.tiskarId = tiskarId;
        this.mistoVydaniId = mistoVydaniId;
        this.titulniStranka = titulniStranka;
        this.prvniStranka = prvniStranka;
        this.rokVydani = rokVydani;
    }

    public Kniha(Integer idKnihy, String carovyKod, String nazev, String signatura, String popisKnihy, Autor autorId, Kategorie kategorieId, Tiskar tiskarId, MistoVydani mistoVydaniId, int titulniStranka, int prvniStranka, RokVydani rokVydani) {
        this.idKnihy = idKnihy;
        this.carovyKod = carovyKod;
        this.nazev = nazev;
        this.signatura = signatura;
        this.popisKnihy = popisKnihy;
        this.autorId = autorId;
        this.kategorieId = kategorieId;
        this.tiskarId = tiskarId;
        this.mistoVydaniId = mistoVydaniId;
        setTitulniStranka(titulniStranka);
        setPrvniStranka(prvniStranka);
        this.rokVydani = rokVydani;
    }

    public MistoVydani getMistoVydaniId() {
        return mistoVydaniId;
    }

    public void setMistoVydaniId(MistoVydani mistoVydaniId) {
        this.mistoVydaniId = mistoVydaniId;
    }

    public Integer getId() {
        return idKnihy;
    }

    public void setIdKnihy(Integer idKnihy) {
        this.idKnihy = idKnihy;
    }

    public String getCarovyKod() {
        return carovyKod;
    }

    public void setCarovyKod(String carovyKod) {
        this.carovyKod = carovyKod;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getSignatura() {
        return signatura;
    }

    public void setSignatura(String signatura) {
        this.signatura = signatura;
    }

    public String getPopisKnihy() {
        return popisKnihy;
    }

    public void setPopisKnihy(String popisKnihy) {
        this.popisKnihy = popisKnihy;
    }

    public Autor getAutorId() {
        return autorId;
    }

    public void setAutorId(Autor autorId) {
        this.autorId = autorId;
    }

    public Kategorie getKategorieId() {
        return kategorieId;
    }

    public void setKategorieId(Kategorie kategorieId) {
        this.kategorieId = kategorieId;
    }

    public Tiskar getTiskarId() {
        return tiskarId;
    }

    public void setTiskarId(Tiskar tiskarId) {
        this.tiskarId = tiskarId;
    }

    public Obrazky getTitulniStranka() {
        return titulniStranka;
    }

    public void setTitulniStranka(Obrazky titulniStranka) {
        this.titulniStranka = titulniStranka;
    }

    public Obrazky getPrvniStranka() {
        return prvniStranka;
    }

    public void setPrvniStranka(Obrazky prvniStranka) {
        this.prvniStranka = prvniStranka;
    }

    public void setTitulniStranka(Integer titulniStranka) {
        this.titulniStranka = Select.getObrazek(titulniStranka);
    }

    public void setPrvniStranka(Integer prvniStranka) {
        this.prvniStranka = Select.getObrazek(prvniStranka);
    }

    public RokVydani getRokVydani() {
        return rokVydani;
    }

    public void setRokVydani(RokVydani rokVydani) {
        this.rokVydani = rokVydani;
    }

    @Override
    public String toString() {
        return idKnihy + ": " + nazev + " (" + autorId.getPrijmeniAutora() + " " + autorId.getJmenoAutora() + ")";
    }

}
