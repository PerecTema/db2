/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

import java.io.InputStream;
import javafx.scene.image.Image;

/**
 *
 * @author Sarlota
 */
public class Obrazky {

    public static final String TABLE = "SEM_OBRAZKY";
    public static final String COL_ID = "ID_OBRAZKU";
    public static final String COL_POPIS = "POPIS_OBRAZKU";
    public static final String COL_OBRAZEK = "OBRAZEK";
    public static final String COL_NAZEV = "NAZEV_OBRAZKU";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";

    private Integer id;
    private String popis;
    private String nazev;
    private byte[] obrazek;

    public Obrazky(Integer id, String popis, String nazev, byte[] obrazek) {
        this.id = id;
        this.popis = popis;
        this.nazev = nazev;
        this.obrazek = obrazek;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public byte[] getObrazek() {
        return obrazek;
    }

    public void setObrazek(byte[] obrazek) {
        this.obrazek = obrazek;
    }

}
