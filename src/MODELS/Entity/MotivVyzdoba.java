/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class MotivVyzdoba {

    public static final String TABLE = "SEM_MOTIV_VYZDOBA";
    public static final String COL_MOTIV_ID = "MOTIV_ID";
    public static final String COL_VYZDOBA_ID = "VYZDOBA_ID";
    
    private Motiv motivId;
    private Vyzdoba vyzdobaId;

    public MotivVyzdoba(Motiv motivId, Vyzdoba vyzdobaId) {
        this.motivId = motivId;
        this.vyzdobaId = vyzdobaId;
    }

    public Motiv getMotivId() {
        return motivId;
    }

    public void setMotivId(Motiv motivId) {
        this.motivId = motivId;
    }

    public Vyzdoba getVyzdobaId() {
        return vyzdobaId;
    }

    public void setVyzdobaId(Vyzdoba vyzdobaId) {
        this.vyzdobaId = vyzdobaId;
    }

}
