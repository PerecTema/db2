/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class Motiv {
    
    public static final String TABLE = "SEM_MOTIV";
    public static final String COL_ID = "ID_MOTIVU";
    public static final String COL_NAZEV_MOTIVU = "NAZEV_MOTIVU";
    public static final String COL_POPIS_MOTIVU = "POPIS_MOTIVU";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";
    
    private Integer idMotivu;
    private String nazevMotivu;
    private String popisMotivu;

    public Motiv(Integer idMotivu, String nazevMotivu, String popisMotivu) {
        this.idMotivu = idMotivu;
        this.nazevMotivu = nazevMotivu;
        this.popisMotivu = popisMotivu;
    }

    public Integer getIdMotivu() {
        return idMotivu;
    }

    public void setIdMotivu(Integer idMotivu) {
        this.idMotivu = idMotivu;
    }

    public String getNazevMotivu() {
        return nazevMotivu;
    }

    public void setNazevMotivu(String nazevMotivu) {
        this.nazevMotivu = nazevMotivu;
    }

    public String getPopisMotivu() {
        return popisMotivu;
    }

    public void setPopisMotivu(String popisMotivu) {
        this.popisMotivu = popisMotivu;
    }

    @Override
    public String toString() {
        return nazevMotivu + " " + popisMotivu;
    }


    
    
}
