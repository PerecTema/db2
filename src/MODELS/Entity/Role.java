/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class Role {

    private Integer idRole;
    private String nazevRole;

    public static final String TABLE = "SEM_ROLE";
    public static final String COL_ID = "ID_ROLE";
    public static final String COL_NAZEV = "NAZEV_ROLE";

    public Role(Integer idRole, String nazevRole) {
        this.idRole = idRole;
        this.nazevRole = nazevRole;
    }

    public Integer getIdRole() {
        return idRole;
    }

    public void setIdRole(Integer idRole) {
        this.idRole = idRole;
    }

    public String getNazevRole() {
        return nazevRole;
    }

    public void setNazevRole(String nazevRole) {
        this.nazevRole = nazevRole;
    }

    @Override
    public String toString() {
        return nazevRole;
    }

}
