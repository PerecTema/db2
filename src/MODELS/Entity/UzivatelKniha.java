/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class UzivatelKniha {

    public static final String TABLE = "SEM_UZIVATEL_KNIHA";
    public static final String COL_ID_TISKARE = "UZIVATEL_ID";
    public static final String COL_NAZEV_TISKARE = "KNIHA_ID";
    
    private Uzivatel uzivatelId;
    private Kniha knihaId;

    public UzivatelKniha(Uzivatel uzivatelId, Kniha knihaId) {
        this.uzivatelId = uzivatelId;
        this.knihaId = knihaId;
    }

    public Uzivatel getUzivatelId() {
        return uzivatelId;
    }

    public void setUzivatelId(Uzivatel uzivatelId) {
        this.uzivatelId = uzivatelId;
    }

    public Kniha getKnihaId() {
        return knihaId;
    }

    public void setKnihaId(Kniha knihaId) {
        this.knihaId = knihaId;
    }

}
