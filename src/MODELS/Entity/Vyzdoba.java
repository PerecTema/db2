/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

import MODELS.GRUD.Select;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Sarlota
 */
public class Vyzdoba {

    public static final String TABLE = "SEM_VYZDOBA";
    public static final String COL_ID = "ID_VYZDOBY";
    public static final String COL_NAZEV = "NAZEV";
    public static final String COL_POPIS = "POPIS";
    public static final String COL_CISLO_STRANKY = "CISLO_STRANKY";
    public static final String COL_VELIKOST_SIRKA = "VELIKOST_SIRKA";
    public static final String COL_VELIKOST_VYSKA = "VELIKOST_VYSKA";
    public static final String COL_ROK_ID = "ROK_ID";
    public static final String COL_KNIHA_ID = "KNIHA_ID";
    public static final String COL_POZICE_ID = "POZICE_ID";
    public static final String COL_AUTOR_ID = "AUTOR_ID";
    public static final String COL_TISKAR_ID = "TISKAR_ID";
    public static final String COL_PARENT_ID = "PARENT_ID";

    private Integer idVyzdoby;
    private String nazevVyzdoby;
    private String popisVyzdoby;
    private Integer cisloStranky;
    private Integer velikostSirka;
    private Integer velikostVyska;
    private RokVydani rokId;
    private Kniha knihaId;
    private PoziceVyzdoby poziceId;
    private Autor autorId;
    private Tiskar tiskarId;
    private Integer parentId = null;

    public Vyzdoba(Integer idVyzdoby, String nazevVyzdoby, String popisVyzdoby, Integer cisloStranky, Integer velikostSirka, Integer velikostVyska, RokVydani rokId, Kniha knihaId, PoziceVyzdoby poziceId, Autor autorId, Tiskar tiskarId, Integer parentId) {
        this.idVyzdoby = idVyzdoby;
        this.nazevVyzdoby = nazevVyzdoby;
        this.popisVyzdoby = popisVyzdoby;
        this.cisloStranky = cisloStranky;
        this.velikostSirka = velikostSirka;
        this.velikostVyska = velikostVyska;
        this.rokId = rokId;
        this.knihaId = knihaId;
        this.poziceId = poziceId;
        this.autorId = autorId;
        this.tiskarId = tiskarId;
        this.parentId = parentId;
    }

    public Integer getIdVyzdoby() {
        return idVyzdoby;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public ObservableList<Vyzdoba> getChildren() {
        ObservableList<Vyzdoba> list = FXCollections.observableArrayList();

        return list;
    }

    public ObservableList<Motiv> getMotivy() {
        ObservableList<Motiv> list = FXCollections.observableArrayList();
        try {
            list = Select.getMotivyVyzdoby(this.idVyzdoby);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public void setIdVyzdoby(Integer idVyzdoby) {
        this.idVyzdoby = idVyzdoby;
    }

    public String getNazevVyzdoby() {
        return nazevVyzdoby;
    }

    public void setNazevVyzdoby(String nazevVyzdoby) {
        this.nazevVyzdoby = nazevVyzdoby;
    }

    public String getPopisVyzdoby() {
        return popisVyzdoby;
    }

    public void setPopisVyzdoby(String popisVyzdoby) {
        this.popisVyzdoby = popisVyzdoby;
    }

    public Integer getCisloStranky() {
        return cisloStranky;
    }

    public void setCisloStranky(Integer cisloStranky) {
        this.cisloStranky = cisloStranky;
    }

    public Integer getVelikostSirka() {
        return velikostSirka;
    }

    public void setVelikostSirka(Integer velikostSirka) {
        this.velikostSirka = velikostSirka;
    }

    public Integer getVelikostVyska() {
        return velikostVyska;
    }

    public void setVelikostVyska(Integer velikostVyska) {
        this.velikostVyska = velikostVyska;
    }

    public RokVydani getRokId() {
        return rokId;
    }

    public void setRokId(RokVydani rokId) {
        this.rokId = rokId;
    }

    public Kniha getKnihaId() {
        return knihaId;
    }

    public void setKnihaId(Kniha knihaId) {
        this.knihaId = knihaId;
    }

    public PoziceVyzdoby getPoziceId() {
        return poziceId;
    }

    public void setPoziceId(PoziceVyzdoby poziceId) {
        this.poziceId = poziceId;
    }

    public Autor getAutorId() {
        return autorId;
    }

    public void setAutorId(Autor autorId) {
        this.autorId = autorId;
    }

    public Tiskar getTiskarId() {
        return tiskarId;
    }

    public void setTiskarId(Tiskar tiskarId) {
        this.tiskarId = tiskarId;
    }

    @Override
    public String toString() {
        return nazevVyzdoby + " " + poziceId;
    }

}
