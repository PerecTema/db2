/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class PoziceVyzdoby {

    public static final String TABLE = "SEM_POZICE_VYZDOBY";
    public static final String COL_ID = "ID_POZICE";
    public static final String COL_NAZEV_POZICE = "NAZEV_POZICE";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";

    private Integer idPozice;
    private String nazevPozice;

    public PoziceVyzdoby(Integer idPozice, String nazevPozice) {
        this.idPozice = idPozice;
        this.nazevPozice = nazevPozice;
    }

    public Integer getIdPozice() {
        return idPozice;
    }

    public void setIdPozice(Integer idPozice) {
        this.idPozice = idPozice;
    }

    public String getNazevPozice() {
        return nazevPozice;
    }

    public void setNazevPozice(String nazevPozice) {
        this.nazevPozice = nazevPozice;
    }

    @Override
    public String toString() {
        return nazevPozice;
    }
}
