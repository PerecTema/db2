/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class Uzivatel{

    public static final String TABLE = "SEM_UZIVATEL";
    public static final String COL_ID = "ID_UZIV";
    public static final String COL_JMENO = "JMENO_UZIV";
    public static final String COL_PRIJMENI = "PRIJMENI_UZIV";
    public static final String COL_HESLO = "PSWD";
    public static final String COL_ROLE = "ROLE_ID";
    public static final String COL_EMAIL = "EMAIL";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";

    private Integer idUzivatele;
    private String jmenoUzivatele;
    private String prijmeniUzivatele;
    private String email;
    private Role role;

    public Uzivatel(Integer idUzivatele, String jmenoUzivatele, String prijmeniUzivatele, String email, Role roleId) {
        this.idUzivatele = idUzivatele;
        this.jmenoUzivatele = jmenoUzivatele;
        this.prijmeniUzivatele = prijmeniUzivatele;
        this.email = email;
        this.role = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdUzivatele() {
        return idUzivatele;
    }

    public void setIdUzivatele(Integer idUzivatele) {
        this.idUzivatele = idUzivatele;
    }

    public String getJmenoUzivatele() {
        return jmenoUzivatele;
    }

    public void setJmenoUzivatele(String jmenoUzivatele) {
        this.jmenoUzivatele = jmenoUzivatele;
    }

    public String getPrijmeniUzivatele() {
        return prijmeniUzivatele;
    }

    public void setPrijmeniUzivatele(String prijmeniUzivatele) {
        this.prijmeniUzivatele = prijmeniUzivatele;
    }

    public String getZkraceneJmenoUzivatele() {
        return email;
    }

    public void setZkraceneJmenoUzivatele(String zkraceneJmenoUzivatele) {
        this.email = zkraceneJmenoUzivatele;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role roleId) {
        this.role = roleId;
    }

    @Override
    public String toString() {
        return idUzivatele + ": " + prijmeniUzivatele + " " + jmenoUzivatele;
    }

}
