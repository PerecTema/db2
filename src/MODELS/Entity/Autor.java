/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class Autor {

    public static final String TABLE = "SEM_AUTOR";
    public static final String COL_ID = "ID_AUTORA";
    public static final String COL_JMENO = "JMENO_AUTORA";
    public static final String COL_PRIJMENI = "PRIJMENI_AUTORA";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";

    private Integer idAutora;
    private String jmenoAutora;
    private String prijmeniAutora;

    public Autor(Integer idAutora, String jmenoAutora, String prijmeniAutora) {
        this.idAutora = idAutora;
        this.jmenoAutora = jmenoAutora;
        this.prijmeniAutora = prijmeniAutora;
    }

    public Integer getId() {
        return idAutora;
    }

    public void setIdAutora(Integer idAutora) {
        this.idAutora = idAutora;
    }

    public String getJmenoAutora() {
        return jmenoAutora;
    }

    public void setJmenoAutora(String jmenoAutora) {
        this.jmenoAutora = jmenoAutora;
    }

    public String getPrijmeniAutora() {
        return prijmeniAutora;
    }

    public void setPrijmeniAutora(String prijmeniAutora) {
        this.prijmeniAutora = prijmeniAutora;
    }

    @Override
    public String toString() {
        return prijmeniAutora + " " + jmenoAutora;
    }
}
