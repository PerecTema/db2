/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class Tiskar {

    public static final String TABLE = "SEM_TISKAR";
    public static final String COL_ID = "ID_TISKARE";
    public static final String COL_NAZEV_TISKARE = "NAZEV_TISKARE";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";

    private Integer idTiskare;
    private String nazevTiskare;

    public Tiskar(Integer idTiskare, String nazevTiskare) {
        this.idTiskare = idTiskare;
        this.nazevTiskare = nazevTiskare;
    }

    public Integer getId() {
        return idTiskare;
    }

    public void setIdTiskare(Integer idTiskare) {
        this.idTiskare = idTiskare;
    }

    public String getNazevTiskare() {
        return nazevTiskare;
    }

    public void setNazevTiskare(String nazevTiskare) {
        this.nazevTiskare = nazevTiskare;
    }

    @Override
    public String toString() {
        return nazevTiskare;
    }

}
