/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.Entity;

/**
 *
 * @author Sarlota
 */
public class Kategorie {

    public static final String TABLE = "SEM_KATEGORIE";
    public static final String COL_ID = "ID_KATEGORIE";
    public static final String COL_NAZEV = "NAZEV_KAT";
    public static final String SEQENCE = "UZIVATEL_ID_SEQ";

    private Integer idKategorie;
    private String nazevKategorie;

    public Kategorie(Integer idKategorie, String nazevKategorie) {
        this.idKategorie = idKategorie;
        this.nazevKategorie = nazevKategorie;
    }

    public Integer getId() {
        return idKategorie;
    }

    public void setIdKategorie(Integer idKategorie) {
        this.idKategorie = idKategorie;
    }

    public String getNazevKategorie() {
        return nazevKategorie;
    }

    public void setNazevKategorie(String nazevKategorie) {
        this.nazevKategorie = nazevKategorie;
    }

    @Override
    public String toString() {
        return nazevKategorie;
    }

}
