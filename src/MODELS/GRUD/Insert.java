/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.GRUD;

import MODELS.Entity.*;
import MODELS.OracleConnector;
import MODELS.UserAuth;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Artem
 */
public class Insert {

    public static Uzivatel insertUser(Uzivatel user, String pswd) throws SQLException {
        if (!UserAuth.tryDo("insertUser")) {
            return user;
        }
        String sql = "INSERT INTO " + OracleConnector.prefix + "." + Uzivatel.TABLE
                + " (" + Uzivatel.COL_ID + ", " + Uzivatel.COL_JMENO + ", " + Uzivatel.COL_PRIJMENI + ", " + Uzivatel.COL_HESLO + ", " + Uzivatel.COL_ROLE + ", " + Uzivatel.COL_EMAIL + ") VALUES "
                + "(null, ?, ?, ?, ?, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, user.getJmenoUzivatele());
        ps.setString(i++, user.getPrijmeniUzivatele());
        ps.setString(i++, pswd);
        ps.setInt(i++, user.getRole().getIdRole());
        ps.setString(i++, user.getEmail());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.execute();
        conn.commit();
        return user;
    }

    public static void insertBook(Kniha kniha) throws SQLException, IOException {
        if (!UserAuth.tryDo("insertBook")) {
            return;
        }
        String sql = "{CALL ADD_BOOK(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareCall(sql);
        ps.setString(i++, kniha.getCarovyKod());
        ps.setString(i++, kniha.getSignatura());
        ps.setInt(i++, kniha.getAutorId().getId());
        ps.setInt(i++, kniha.getTiskarId().getId());
        ps.setInt(i++, kniha.getMistoVydaniId().getId());
        ps.setString(i++, kniha.getNazev());
        ps.setInt(i++, kniha.getKategorieId().getId());
        ps.setString(i++, kniha.getPopisKnihy());
        ps.setInt(i++, kniha.getRokVydani().getRokZacatek());
        ps.setInt(i++, kniha.getRokVydani().getRokKonec());

        //obrazek 1
        ps.setString(i++, kniha.getTitulniStranka().getPopis());
        ps.setString(i++, kniha.getTitulniStranka().getNazev());
        ps.setBytes(i++, kniha.getTitulniStranka().getObrazek());
        //obrazek 2
        ps.setString(i++, kniha.getPrvniStranka().getPopis());
        ps.setString(i++, kniha.getPrvniStranka().getNazev());
        ps.setBytes(i++, kniha.getPrvniStranka().getObrazek());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertObrazekVyzdoby(Vyzdoba vyzdoba, Obrazky obrazek) throws SQLException, IOException {
        if (!UserAuth.tryDo("insertObrazekVyzdoby")) {
            //return;
        }
        String sql = "{CALL ADD_IMAGE_VYZDOBA(?,?,?,?)}";
        int i = 1;
        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareCall(sql);
        ps.setInt(i++, vyzdoba.getIdVyzdoby());
        ps.setString(i++, obrazek.getNazev());
        ps.setString(i++, obrazek.getPopis());
        ps.setBytes(i++, obrazek.getObrazek());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertTiskar(Tiskar tiskar) throws SQLException {
        if (!UserAuth.tryDo("insertTiskar")) {
            return;
        }
        String sql = "INSERT INTO " + OracleConnector.prefix + "." + Tiskar.TABLE
                + " (" + Tiskar.COL_ID + ", " + Tiskar.COL_NAZEV_TISKARE + ") VALUES "
                + "(null, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, tiskar.getNazevTiskare());

        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertAutor(Autor autor) throws SQLException {
        if (!UserAuth.tryDo("insertAutor")) {
            return;
        }
        String sql = "INSERT INTO " + OracleConnector.prefix + "." + Autor.TABLE
                + " (" + Autor.COL_ID + ", " + Autor.COL_JMENO + ", " + Autor.COL_PRIJMENI + ") VALUES "
                + "(null, ?, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, autor.getJmenoAutora());
        ps.setString(i++, autor.getPrijmeniAutora());

        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertKategorie(Kategorie kategorie) throws SQLException {
        if (!UserAuth.tryDo("insertKategorie")) {
            return;
        }

        String sql = "INSERT INTO " + OracleConnector.prefix + "." + Kategorie.TABLE
                + " (" + Kategorie.COL_ID + ", " + Kategorie.COL_NAZEV + ") VALUES "
                + "(null, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, kategorie.getNazevKategorie());

        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertMistoVydani(MistoVydani mistoVydani) throws SQLException {
        if (!UserAuth.tryDo("insertMistoVydani")) {
            return;
        }
        String sql = "INSERT INTO " + OracleConnector.prefix + "." + MistoVydani.TABLE
                + " (" + MistoVydani.COL_ID + ", " + MistoVydani.COL_JMENO + ") VALUES "
                + "(null, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, mistoVydani.getNazevMista());

        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertPozice(PoziceVyzdoby tiskar) throws SQLException {
        if (!UserAuth.tryDo("insertPozice")) {
            return;
        }
        String sql = "INSERT INTO " + OracleConnector.prefix + "." + PoziceVyzdoby.TABLE
                + " (" + PoziceVyzdoby.COL_ID + ", " + PoziceVyzdoby.COL_NAZEV_POZICE + ") VALUES "
                + "(null, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, tiskar.getNazevPozice());

        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertMotivVyzdoba(Motiv motiv, Vyzdoba vyzdoba) throws SQLException {
        if (!UserAuth.tryDo("insertMotivVyzdoba")) {
            return;
        }
        String sql = "INSERT INTO " + OracleConnector.prefix + "." + MotivVyzdoba.TABLE
                + " (" + MotivVyzdoba.COL_MOTIV_ID + ", " + MotivVyzdoba.COL_VYZDOBA_ID + ") VALUES "
                + "(?, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(i++, motiv.getIdMotivu());
        ps.setInt(i++, vyzdoba.getIdVyzdoby());

        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertThem(Motiv motiv) throws SQLException {
        if (!UserAuth.tryDo("insertThem")) {
            return;
        }
        String sql = "INSERT INTO " + OracleConnector.prefix + "." + Motiv.TABLE
                + " (" + Motiv.COL_NAZEV_MOTIVU + ", " + Motiv.COL_POPIS_MOTIVU + ") VALUES "
                + "(?, ?)";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, motiv.getNazevMotivu());
        ps.setString(i++, motiv.getPopisMotivu());

        System.out.println(sql);
        ps.execute();
        conn.commit();
    }

    public static void insertVyzdoba(Vyzdoba vyzdoba, Integer knihaId) throws SQLException {
        if (!UserAuth.tryDo("insertVyzdoba")) {
            return;
        }
        String sql = "{CALL PCK_VYZDOBY.ADD_VYZDOBA(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareCall(sql);

        ps.setString(i++, vyzdoba.getNazevVyzdoby());
        ps.setString(i++, vyzdoba.getPopisVyzdoby());
        ps.setInt(i++, knihaId);
        ps.setInt(i++, vyzdoba.getAutorId().getId());
        ps.setInt(i++, vyzdoba.getCisloStranky());
        ps.setInt(i++, vyzdoba.getPoziceId().getIdPozice());
        ps.setInt(i++, vyzdoba.getTiskarId().getId());
        ps.setInt(i++, vyzdoba.getVelikostSirka());
        ps.setInt(i++, vyzdoba.getVelikostVyska());
        ps.setInt(i++, vyzdoba.getRokId().getRokZacatek());
        ps.setInt(i++, vyzdoba.getRokId().getRokKonec());
        ps.setInt(i++, vyzdoba.getParentId());
        System.out.println(sql);
        ps.execute();
        conn.commit();
    }
}
