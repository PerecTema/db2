/*
create or replace PROCEDURE ADD_BOOK (
    P_NAZEV VARCHAR2, P_POPIS VARCHAR2, P_CISLO_STRANKY NUMBER, P_VELIKOST_SIRKA NUMBER, P_VELIKOST_VYSKA NUMBER,
ROK_ID
KNIHA_ID
POZICE_ID
AUTOR_ID
TISKAR_ID
    )
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.GRUD;

import MODELS.Entity.Kniha;
import MODELS.Entity.Obrazky;
import MODELS.Entity.Uzivatel;
import MODELS.OracleConnector;
import MODELS.UserAuth;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Artem
 */
public class Delete {

    public static void del(String table, String idColumn, Integer id) throws SQLException {
        if (!UserAuth.tryDo("del" + table)) {
            return;
        }
        String SQL = "DELETE FROM " + OracleConnector.prefix + "." + table + " WHERE " + idColumn + " = ?";

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(SQL);
        ps.setInt(1, id);

        // execute insert SQL stetement
        System.out.println(SQL);
        ps.executeUpdate();
        conn.commit();
    }

    public static void uzivatelDelete(Uzivatel uzivatel) throws SQLException {
        self:
        del(Uzivatel.TABLE, Uzivatel.COL_ID, uzivatel.getIdUzivatele());
    }

    public static void bookDelete(Kniha selectedItem) throws SQLException {
        self:
        del(Kniha.TABLE, Kniha.COL_ID, selectedItem.getId());
    }

    public static void obrazekDelete(Obrazky imageFile) throws SQLException {
        self:
        del(Obrazky.TABLE, Obrazky.COL_ID, imageFile.getId());
    }
}
