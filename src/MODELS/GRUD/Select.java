/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.GRUD;

import MODELS.Entity.Autor;
import MODELS.Entity.Kategorie;
import MODELS.Entity.Kniha;
import MODELS.Entity.MistoVydani;
import MODELS.Entity.Motiv;
import MODELS.Entity.MotivVyzdoba;
import MODELS.Entity.Obrazky;
import MODELS.Entity.PoziceVyzdoby;
import MODELS.Entity.RokVydani;
import MODELS.Entity.Role;
import MODELS.Entity.Tiskar;
import MODELS.Entity.Uzivatel;
import MODELS.Entity.Vyzdoba;
import MODELS.OracleConnector;
import java.io.ByteArrayInputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author Artem
 */
public class Select {

    public static ObservableList<Uzivatel> getUsers() throws SQLException {
        ObservableList<Uzivatel> listOfUsers = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select " + Uzivatel.TABLE + ".*, " + Role.TABLE + ".* from " + OracleConnector.prefix + "." + Uzivatel.TABLE
                + " inner join  " + OracleConnector.prefix + "." + Role.TABLE + "  ON " + Role.TABLE + "." + Role.COL_ID + " = " + Uzivatel.TABLE + "." + Uzivatel.COL_ROLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            listOfUsers.add(new Uzivatel(rs.getInt(Uzivatel.COL_ID), rs.getString(Uzivatel.COL_JMENO), rs.getString(Uzivatel.COL_PRIJMENI), rs.getString(Uzivatel.COL_EMAIL), new Role(rs.getInt(Role.COL_ID), rs.getString(Role.COL_NAZEV))));
        }
        return listOfUsers;
    }

    public static ObservableList<Role> getRoles() throws SQLException {
        ObservableList<Role> listOfUsers = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * from " + OracleConnector.prefix + "." + Role.TABLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            listOfUsers.add(new Role(rs.getInt(Role.COL_ID), rs.getString(Role.COL_NAZEV)));
        }
        return listOfUsers;
    }

    public static Integer getSeq(String name) throws SQLException {
        Connection conn;
        PreparedStatement ps;
        String select = "select " + name + ".nextval from DUAL";
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            return rs.getInt(1);
        }
        return null;
    }

    public static ObservableList<Kniha> getBooksWhere(String where) throws SQLException {
        ObservableList<Kniha> listOfUsers = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * "
                + " from " + OracleConnector.prefix + "." + Kniha.TABLE
                + " inner join  " + OracleConnector.prefix + "." + RokVydani.TABLE + "  ON " + RokVydani.TABLE + "." + RokVydani.COL_ID + " = " + Kniha.TABLE + "." + Kniha.COL_ROK_VYDANI
                + " inner join  " + OracleConnector.prefix + "." + Tiskar.TABLE + "  ON " + Tiskar.TABLE + "." + Tiskar.COL_ID + " = " + Kniha.TABLE + "." + Kniha.COL_TISKAR_ID
                + " inner join  " + OracleConnector.prefix + "." + Kategorie.TABLE + "  ON " + Kategorie.TABLE + "." + Kategorie.COL_ID + " = " + Kniha.TABLE + "." + Kniha.COL_KATEGORIE_ID
                + " inner join  " + OracleConnector.prefix + "." + MistoVydani.TABLE + "  ON " + MistoVydani.TABLE + "." + MistoVydani.COL_ID + " = " + Kniha.TABLE + "." + Kniha.COL_MISTO_VYDANI_ID
                + " inner join  " + OracleConnector.prefix + "." + Obrazky.TABLE + "  ON " + Obrazky.TABLE + "." + Obrazky.COL_ID + " = " + Kniha.TABLE + "." + Kniha.COL_PRVNI_STR_ID
                + " inner join  " + OracleConnector.prefix + "." + Obrazky.TABLE + "  ON " + Obrazky.TABLE + "." + Obrazky.COL_ID + " = " + Kniha.TABLE + "." + Kniha.COL_TITUL_STR_ID
                + " inner join  " + OracleConnector.prefix + "." + Autor.TABLE + "  ON " + Autor.TABLE + "." + Autor.COL_ID + " = " + Kniha.TABLE + "." + Kniha.COL_AUTOR_ID;
        if (!"".equals(where)) {
            select
                    += " WHERE " + RokVydani.COL_ROK_KONEC + " LIKE '%" + where + "%' OR " + RokVydani.COL_ROK_KONEC + " LIKE '%" + where + "%' OR "
                    + Tiskar.COL_NAZEV_TISKARE + " LIKE '%" + where + "%' OR "
                    + Kategorie.COL_NAZEV + " LIKE '%" + where + "%' OR "
                    + MistoVydani.COL_JMENO + " LIKE '%" + where + "%' OR "
                    //+ Obrazky.COL_NAZEV + " LIKE '%" + where + "%' OR "
                    //+ Obrazky.COL_POPIS + " LIKE '%" + where + "%' OR "
                    + Autor.COL_JMENO + " LIKE '%" + where + "%' OR "
                    + Autor.COL_PRIJMENI + " LIKE '%" + where + "%' ";
        }
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            listOfUsers.add(
                    new Kniha(
                            rs.getInt(Kniha.COL_ID),
                            rs.getString(Kniha.COL_EAN),
                            rs.getString(Kniha.COL_NAZEV),
                            rs.getString(Kniha.COL_SIGNATURA),
                            rs.getString(Kniha.COL_SIGNATURA),
                            new Autor(
                                    rs.getInt(Autor.COL_ID),
                                    rs.getString(Autor.COL_JMENO),
                                    rs.getString(Autor.COL_PRIJMENI)
                            ),
                            new Kategorie(
                                    rs.getInt(Kategorie.COL_ID),
                                    rs.getString(Kategorie.COL_NAZEV)
                            ),
                            new Tiskar(
                                    rs.getInt(Tiskar.COL_ID),
                                    rs.getString(Tiskar.COL_NAZEV_TISKARE)
                            ),
                            new MistoVydani(
                                    rs.getInt(MistoVydani.COL_ID),
                                    rs.getString(MistoVydani.COL_JMENO)
                            ),
                            new Obrazky(rs.getInt(19 + 3), rs.getString(20 + 3), rs.getString(22 + 3), rs.getBytes(21 + 3)),
                            new Obrazky(rs.getInt(23 + 3), rs.getString(24 + 3), rs.getString(26 + 3), rs.getBytes(25 + 3)),
                            new RokVydani(
                                    rs.getInt(RokVydani.COL_ID),
                                    rs.getInt(RokVydani.COL_ROK_ZACATEK),
                                    rs.getInt(RokVydani.COL_ROK_KONEC)
                            )
                    )
            );
        }
        return listOfUsers;
    }

    public static ObservableList<Kniha> getBooks() throws SQLException {
        return Select.getBooksWhere("");
    }

    public static ObservableList<Tiskar> getTiskare() throws SQLException {
        ObservableList<Tiskar> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * from " + OracleConnector.prefix + "." + Tiskar.TABLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Tiskar(rs.getInt(Tiskar.COL_ID), rs.getString(Tiskar.COL_NAZEV_TISKARE)));
        }
        return list;
    }

    public static ObservableList<Motiv> getMotivy() throws SQLException {
        ObservableList<Motiv> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * from " + OracleConnector.prefix + "." + Motiv.TABLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Motiv(rs.getInt(Motiv.COL_ID), rs.getString(Motiv.COL_NAZEV_MOTIVU), rs.getString(Motiv.COL_POPIS_MOTIVU)));
        }
        return list;
    }

    public static ObservableList<Obrazky> getSkenyVyzdoby(int idVyzdoby) throws SQLException {
        ObservableList<Obrazky> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select " + Obrazky.TABLE + ".* from " + OracleConnector.prefix + ".SEM_OBRAZEK_VYZDOBA "
                + " inner join  " + Obrazky.TABLE + "  ON SEM_OBRAZEK_VYZDOBA.OBRAZEK_ID = " + Obrazky.TABLE + "." + Obrazky.COL_ID
                + " WHERE SEM_OBRAZEK_VYZDOBA.VYZDOBA_ID = ?";
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ps.setInt(1, idVyzdoby);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Obrazky(
                    rs.getInt(Obrazky.COL_ID),
                    rs.getString(Obrazky.COL_NAZEV),
                    rs.getString(Obrazky.COL_POPIS),
                    rs.getBytes(Obrazky.COL_OBRAZEK)
            ));
        }
        return list;
    }

    public static ObservableList<Vyzdoba> getVyzdoby(int idKnihi) throws SQLException {
        ObservableList<Vyzdoba> list = FXCollections.observableArrayList();
        try {
            CallableStatement callableStatement;
            String getReports = "{call PCK_VYZDOBY.getVyzdobyKnihi(?,?)}";
            ResultSet reportsResultSet;

            callableStatement = OracleConnector.getConnection().prepareCall(getReports);
            callableStatement.setInt(1, idKnihi);
            callableStatement.registerOutParameter(2, OracleTypes.CURSOR);
            callableStatement.execute();
            reportsResultSet = (ResultSet) callableStatement.getObject(2);
            while (reportsResultSet.next()) {
                list.add(new Vyzdoba(
                        reportsResultSet.getInt(Vyzdoba.COL_ID),
                        reportsResultSet.getString(Vyzdoba.COL_NAZEV),
                        reportsResultSet.getString(Vyzdoba.COL_POPIS),
                        reportsResultSet.getInt(Vyzdoba.COL_CISLO_STRANKY),
                        reportsResultSet.getInt(Vyzdoba.COL_VELIKOST_SIRKA),
                        reportsResultSet.getInt(Vyzdoba.COL_VELIKOST_VYSKA),
                        new RokVydani(
                                reportsResultSet.getInt(Vyzdoba.COL_ROK_ID),
                                reportsResultSet.getInt(RokVydani.COL_ROK_ZACATEK),
                                reportsResultSet.getInt(RokVydani.COL_ROK_KONEC)
                        ),
                        null,//Kniha
                        new PoziceVyzdoby(
                                reportsResultSet.getInt(Vyzdoba.COL_POZICE_ID),
                                reportsResultSet.getString(PoziceVyzdoby.COL_NAZEV_POZICE)
                        ),
                        new Autor(
                                reportsResultSet.getInt(Vyzdoba.COL_AUTOR_ID),
                                reportsResultSet.getString(Autor.COL_JMENO),
                                reportsResultSet.getString(Autor.COL_PRIJMENI)
                        ),
                        new Tiskar(
                                reportsResultSet.getInt(Vyzdoba.COL_TISKAR_ID),
                                reportsResultSet.getString(Tiskar.COL_NAZEV_TISKARE)
                        ),
                        Integer.valueOf(reportsResultSet.getInt(Vyzdoba.COL_PARENT_ID))
                ));
            }
            reportsResultSet.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public static ObservableList<Autor> getAutors() throws SQLException {
        ObservableList<Autor> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * from " + OracleConnector.prefix + "." + Autor.TABLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Autor(rs.getInt(Autor.COL_ID), rs.getString(Autor.COL_JMENO), rs.getString(Autor.COL_PRIJMENI)));
        }
        return list;
    }

    public static ObservableList<MistoVydani> getMistaVydani() throws SQLException {
        ObservableList<MistoVydani> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * from " + OracleConnector.prefix + "." + MistoVydani.TABLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(
                    new MistoVydani(
                            rs.getInt(MistoVydani.COL_ID),
                            rs.getString(MistoVydani.COL_JMENO)
                    )
            );
        }
        return list;
    }

    public static ObservableList<Kategorie> getKategorie() throws SQLException {
        ObservableList<Kategorie> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * from " + OracleConnector.prefix + "." + Kategorie.TABLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(
                    new Kategorie(rs.getInt(Kategorie.COL_ID),
                            rs.getString(Kategorie.COL_NAZEV))
            );
        }
        return list;
    }

    public static Obrazky getObrazek(Integer id) {
        String select = "SELECT * FROM " + OracleConnector.prefix + "." + Obrazky.TABLE
                + " WHERE " + Obrazky.COL_ID + " = ?";
        try {
            Connection conn = OracleConnector.getConnection();
            PreparedStatement ps = conn.prepareStatement(select);
            ps.setInt(1, id);
            System.out.println(select);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blob blob = rs.getBlob(Obrazky.COL_OBRAZEK);
                byte[] byteImage = blob.getBytes(1, (int) blob.length());
                return new Obrazky(
                        id,
                        rs.getString(Obrazky.COL_POPIS),
                        rs.getString(Obrazky.COL_NAZEV),
                        byteImage
                );
            }
        } catch (SQLException | Error ex) {
            System.out.println(ex.getMessage());
        }
        throw new Error("Obrazek nebyl nalezen!");
    }

    public static ObservableList<PoziceVyzdoby> getPoziceVyzdoby() throws SQLException {
        ObservableList<PoziceVyzdoby> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select * from " + OracleConnector.prefix + "." + PoziceVyzdoby.TABLE;
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(
                    new PoziceVyzdoby(rs.getInt(PoziceVyzdoby.COL_ID),
                            rs.getString(PoziceVyzdoby.COL_NAZEV_POZICE))
            );
        }
        return list;
    }

    public static ObservableList<Motiv> getMotivyVyzdoby(Integer idVyzdoby) throws SQLException {
        ObservableList<Motiv> list = FXCollections.observableArrayList();
        Connection conn;
        PreparedStatement ps;
        String select = "select " + Motiv.TABLE + ".* from " + OracleConnector.prefix + "." + MotivVyzdoba.TABLE
                + " inner join  " + OracleConnector.prefix + "." + Motiv.TABLE + "  ON " + MotivVyzdoba.TABLE + "." + MotivVyzdoba.COL_MOTIV_ID + " = " + Motiv.TABLE + "." + Motiv.COL_ID
                + " WHERE " + MotivVyzdoba.COL_VYZDOBA_ID + "= ?";
        System.out.println(select);
        conn = OracleConnector.getConnection();
        ps = conn.prepareStatement(select);
        ps.setInt(1, idVyzdoby);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            list.add(new Motiv(
                    rs.getInt(Motiv.COL_ID),
                    rs.getString(Motiv.COL_NAZEV_MOTIVU),
                    rs.getString(Motiv.COL_POPIS_MOTIVU)
            ));
        }
        return list;
    }

    public static ObservableList<Vyzdoba> getPotomkyVyzdoby(int idVyzdoby) throws SQLException {
        ObservableList<Vyzdoba> list = FXCollections.observableArrayList();
        try {
            CallableStatement callableStatement;
            String getReports = "{call PCK_VYZDOBY.getPotomkyVyzdoby(?,?)}";
            ResultSet reportsResultSet;

            callableStatement = OracleConnector.getConnection().prepareCall(getReports);
            callableStatement.setInt(1, idVyzdoby);
            callableStatement.registerOutParameter(2, OracleTypes.CURSOR);
            callableStatement.execute();
            reportsResultSet = (ResultSet) callableStatement.getObject(2);
            while (reportsResultSet.next()) {
                list.add(new Vyzdoba(
                        reportsResultSet.getInt(Vyzdoba.COL_ID),
                        reportsResultSet.getString(Vyzdoba.COL_NAZEV),
                        reportsResultSet.getString(Vyzdoba.COL_POPIS),
                        reportsResultSet.getInt(Vyzdoba.COL_CISLO_STRANKY),
                        reportsResultSet.getInt(Vyzdoba.COL_VELIKOST_SIRKA),
                        reportsResultSet.getInt(Vyzdoba.COL_VELIKOST_VYSKA),
                        new RokVydani(
                                reportsResultSet.getInt(Vyzdoba.COL_ROK_ID),
                                reportsResultSet.getInt(RokVydani.COL_ROK_ZACATEK),
                                reportsResultSet.getInt(RokVydani.COL_ROK_KONEC)
                        ),
                        null,//Kniha
                        new PoziceVyzdoby(
                                reportsResultSet.getInt(Vyzdoba.COL_POZICE_ID),
                                reportsResultSet.getString(PoziceVyzdoby.COL_NAZEV_POZICE)
                        ),
                        new Autor(
                                reportsResultSet.getInt(Vyzdoba.COL_AUTOR_ID),
                                reportsResultSet.getString(Autor.COL_JMENO),
                                reportsResultSet.getString(Autor.COL_PRIJMENI)
                        ),
                        new Tiskar(
                                reportsResultSet.getInt(Vyzdoba.COL_TISKAR_ID),
                                reportsResultSet.getString(Tiskar.COL_NAZEV_TISKARE)
                        ),
                        reportsResultSet.getInt(Vyzdoba.COL_PARENT_ID)
                ));
            }
            reportsResultSet.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
