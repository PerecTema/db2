/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS.GRUD;

import MODELS.Entity.*;
import MODELS.OracleConnector;
import MODELS.UserAuth;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author Artem
 */
public class Update {

    public static Uzivatel updateUser(Uzivatel user, String pswd) throws SQLException {
        if (!UserAuth.tryDo("updateUser")) {
            return user;
        }
        String sql = "UPDATE " + OracleConnector.prefix + "." + Uzivatel.TABLE
                + " SET "
                + Uzivatel.COL_JMENO + " = ?, "
                + Uzivatel.COL_PRIJMENI + " = ?, "
                + Uzivatel.COL_HESLO + " = ?, "
                + Uzivatel.COL_ROLE + " = ?, "
                + Uzivatel.COL_EMAIL + " = ?"
                + " WHERE " + Uzivatel.COL_ID + " = ?";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, user.getJmenoUzivatele());
        ps.setString(i++, user.getPrijmeniUzivatele());
        ps.setString(i++, pswd);
        ps.setInt(i++, user.getRole().getIdRole());
        ps.setString(i++, user.getEmail());
        ps.setInt(i++, user.getIdUzivatele());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.executeUpdate();
        conn.commit();

        return user;
    }

    public static Obrazky updateObrazek(Obrazky obrazek) throws SQLException {
        if (!UserAuth.tryDo("updateObrazek")) {
            return obrazek;
        }
        String sql = "UPDATE " + OracleConnector.prefix + "." + Obrazky.TABLE
                + " SET "
                + Obrazky.COL_NAZEV + " = ?, "
                + Obrazky.COL_OBRAZEK + " = ?, "
                + Obrazky.COL_POPIS + " = ? "
                + " WHERE " + Obrazky.COL_ID + " = ?";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, obrazek.getNazev());
        ps.setBytes(i++, obrazek.getObrazek());
        ps.setString(i++, obrazek.getPopis());
        ps.setInt(i++, obrazek.getId());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.executeUpdate();
        conn.commit();

        return obrazek;
    }

    public static RokVydani updateRokVydani(RokVydani rokVydani) throws SQLException {
        if (!UserAuth.tryDo("updateRokVydani")) {
            return rokVydani;
        }
        String sql = "UPDATE " + OracleConnector.prefix + "." + RokVydani.TABLE
                + " SET "
                + RokVydani.COL_ROK_ZACATEK + " = ?, "
                + RokVydani.COL_ROK_KONEC + " = ? "
                + " WHERE " + RokVydani.COL_ID + " = ?";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(i++, rokVydani.getRokZacatek());
        ps.setInt(i++, rokVydani.getRokKonec());
        ps.setInt(i++, rokVydani.getId());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.executeUpdate();
        conn.commit();

        return rokVydani;
    }

    public static Vyzdoba updateVyzdoba(Vyzdoba vyzdoba) throws SQLException {
        if (!UserAuth.tryDo("updateBook")) {
            return vyzdoba;
        }
        String sql = "UPDATE " + OracleConnector.prefix + "." + Vyzdoba.TABLE
                + " SET "
                + Vyzdoba.COL_AUTOR_ID + " = ?, "
                + Vyzdoba.COL_CISLO_STRANKY + " = ?, "
                + Vyzdoba.COL_POPIS + " = ?, "
                + Vyzdoba.COL_NAZEV + " = ?, "
                + Vyzdoba.COL_POZICE_ID + " = ?, "
                + Vyzdoba.COL_TISKAR_ID + " = ?, "
                + Vyzdoba.COL_VELIKOST_SIRKA + " = ?, "
                + Vyzdoba.COL_VELIKOST_VYSKA + " = ?, "
                + Vyzdoba.COL_PARENT_ID + " = ? "
                + " WHERE " + Vyzdoba.COL_ID + " = ?";
        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setInt(i++, vyzdoba.getAutorId().getId());
        ps.setInt(i++, vyzdoba.getCisloStranky());
        ps.setString(i++, vyzdoba.getPopisVyzdoby());
        ps.setString(i++, vyzdoba.getNazevVyzdoby());
        ps.setInt(i++, vyzdoba.getPoziceId().getIdPozice());
        ps.setInt(i++, vyzdoba.getTiskarId().getId());
        ps.setInt(i++, vyzdoba.getVelikostSirka());
        ps.setInt(i++, vyzdoba.getVelikostVyska());
        ps.setInt(i++, vyzdoba.getParentId());
        ps.setInt(i++, vyzdoba.getIdVyzdoby());

        Update.updateRokVydani(vyzdoba.getRokId());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.executeUpdate();
        conn.commit();

        return vyzdoba;
    }

    public static Kniha updateBook(Kniha kniha) throws SQLException {
        if (!UserAuth.tryDo("updateBook")) {
            return kniha;
        }
        String sql = "UPDATE " + OracleConnector.prefix + "." + Kniha.TABLE
                + " SET "
                + Kniha.COL_NAZEV + " = ?, "
                + Kniha.COL_POPIS + " = ?, "
                + Kniha.COL_SIGNATURA + " = ?, "
                + Kniha.COL_KATEGORIE_ID + " = ?, "
                + Kniha.COL_EAN + " = ?, "
                + Kniha.COL_AUTOR_ID + " = ?, "
                + Kniha.COL_TISKAR_ID + " = ?, "
                + Kniha.COL_MISTO_VYDANI_ID + " = ? "
                + " WHERE " + Kniha.COL_ID + " = ?";

        int i = 1;

        Connection conn = OracleConnector.getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(i++, kniha.getNazev());
        ps.setString(i++, kniha.getPopisKnihy());
        ps.setString(i++, kniha.getSignatura());
        ps.setInt(i++, kniha.getKategorieId().getId());
        ps.setString(i++, kniha.getCarovyKod());
        ps.setInt(i++, kniha.getAutorId().getId());
        ps.setInt(i++, kniha.getTiskarId().getId());
        ps.setInt(i++, kniha.getMistoVydaniId().getId());
        ps.setInt(i++, kniha.getId());

        Update.updateRokVydani(kniha.getRokVydani());

        Update.updateObrazek(kniha.getTitulniStranka());
        Update.updateObrazek(kniha.getPrvniStranka());

        // execute insert SQL stetement
        System.out.println(sql);
        ps.executeUpdate();
        conn.commit();

        return kniha;
    }
}
