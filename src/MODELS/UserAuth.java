/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELS;

import MODELS.Entity.Autor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Artem
 */
public class UserAuth {

    private static String ROLE;
    private static String NAME;
    private static String PSWD;
    private static boolean isAuth = false;

    public static String getROLE() {
        return ROLE;
    }

    public static String getNAME() {
        return NAME;
    }

    public static boolean tryDo(String event) {
        try {
            Connection conn;
            PreparedStatement ps;
            String select = "select can_do(?, ?) from dual";
            System.out.println(select);
            conn = OracleConnector.getConnection();
            ps = conn.prepareStatement(select);
            ps.setString(1, event);
            ps.setString(2, UserAuth.getROLE());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1) == 1;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return false;
    }

    public static void login(String login, String pswd) {
        self:
        NAME = login;
        self:
        PSWD = pswd;
        Connection connection;
        try {
            connection = OracleConnector.getConnection();
            isAuth = true;
            return;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        throw new SecurityException("User nemuze byt prihlasen!");
    }

    public static boolean isAuth() {
        return isAuth;
    }
}
