/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import MODELS.Entity.Autor;
import MODELS.Entity.Kategorie;
import MODELS.Entity.Kniha;
import MODELS.Entity.MistoVydani;
import MODELS.Entity.Obrazky;
import MODELS.Entity.RokVydani;
import MODELS.Entity.Tiskar;
import MODELS.GRUD.Delete;
import MODELS.GRUD.Insert;
import MODELS.GRUD.Select;
import MODELS.GRUD.Update;
import MODELS.UserAuth;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Pair;

/**
 * FXML Controller class
 *
 * @author Artem
 */
public class FXMLBooksController implements Initializable {

    @FXML
    private TextField nameFild;
    @FXML
    private ListView<Kniha> booksList;
    @FXML
    private TextField eanFild;
    @FXML
    private TextField signaturaFild;
    @FXML
    private ComboBox<Autor> autorsList;
    @FXML
    private TextArea descriptionField;
    @FXML
    private ComboBox<Tiskar> tiskarList;
    @FXML
    private ChoiceBox<Kategorie> kategorieList;
    @FXML
    private ChoiceBox<MistoVydani> mistoVydaniList;

    public static Kniha vybranaKniha = null;

    @FXML
    private Pane titulImgPan;
    private byte[] titulImg;

    @FXML
    private Pane firstImgPan;
    private byte[] firstImg;

    @FXML
    private TextField rokVydaniStart;

    @FXML
    private TextField rokVydaniKonec;
    @FXML
    private TextField titulImgPopis;
    @FXML
    private TextField firstImgPopis;
    @FXML
    private TextField firstImgName;
    @FXML
    private TextField titulImgName;
    @FXML
    private TextField findField;

    private void loadAutors() {
        try {
            autorsList.setItems(Select.getAutors());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    private void loadTiskare() {
        try {
            tiskarList.setItems(Select.getTiskare());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadMistaVydani() {
        try {
            mistoVydaniList.setItems(Select.getMistaVydani());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadBooks(String where) {

        try {
            booksList.setItems(Select.getBooksWhere(where));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadKategorie() {
        try {
            kategorieList.setItems(Select.getKategorie());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
// The Java 8 way to get the response value (with lambda expression).
        this.loadBooks("");
        this.loadTiskare();
        this.loadAutors();
        this.loadKategorie();
        this.loadMistaVydani();
    }

    @FXML
    private void deleteBookHandle(ActionEvent event) {
        if (!"Admin".equals(UserAuth.getROLE())) {
            System.out.println("nemate paravo");
            //return;
        }
        if (booksList.getSelectionModel().getSelectedItem() == null) {
            return;
        }
        try {
            Delete.bookDelete(booksList.getSelectionModel().getSelectedItem());
            this.loadBooks("");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void saveBookHandle(ActionEvent event) {
        try {
            Kniha book = booksList.getSelectionModel().getSelectedItem();
            if (book == null) {
                return;
            }
            book.setNazev(nameFild.getText());
            book.setCarovyKod(eanFild.getText());
            book.setSignatura(signaturaFild.getText());
            book.setAutorId(autorsList.getSelectionModel().getSelectedItem());
            book.setTiskarId(tiskarList.getSelectionModel().getSelectedItem());
            book.setKategorieId(kategorieList.getSelectionModel().getSelectedItem());
            book.setMistoVydaniId(mistoVydaniList.getSelectionModel().getSelectedItem());
            book.setPopisKnihy(descriptionField.getText());
            book.setPopisKnihy(descriptionField.getText());

            book.getRokVydani().setRokZacatek(Integer.parseInt(rokVydaniStart.getText()));
            book.getRokVydani().setRokKonec(Integer.parseInt(rokVydaniKonec.getText()));

            book.getTitulniStranka().setPopis(titulImgPopis.getText());
            book.getTitulniStranka().setNazev(titulImgName.getText());
            book.getTitulniStranka().setObrazek(titulImg);

            book.getPrvniStranka().setPopis(firstImgPopis.getText());
            book.getPrvniStranka().setNazev(firstImgName.getText());
            book.getPrvniStranka().setObrazek(firstImg);

            Update.updateBook(book);
            this.loadBooks("");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    public void handleChusBook(MouseEvent arg0) {
        Kniha selectedItem = booksList.getSelectionModel().getSelectedItem();
        nameFild.setText(selectedItem.getNazev());
        eanFild.setText(selectedItem.getCarovyKod());
        signaturaFild.setText(selectedItem.getSignatura());
        descriptionField.setText(selectedItem.getPopisKnihy());
        autorsList.getSelectionModel().select(selectedItem.getAutorId());
        kategorieList.getSelectionModel().select(selectedItem.getKategorieId());
        mistoVydaniList.getSelectionModel().select(selectedItem.getMistoVydaniId());
        tiskarList.getSelectionModel().select(selectedItem.getTiskarId());
        rokVydaniStart.setText(String.valueOf(selectedItem.getRokVydani().getRokZacatek()));
        rokVydaniKonec.setText(String.valueOf(selectedItem.getRokVydani().getRokKonec()));
        titulImgName.setText(selectedItem.getTitulniStranka().getNazev());
        titulImgPopis.setText(selectedItem.getTitulniStranka().getPopis());
        firstImgName.setText(selectedItem.getPrvniStranka().getNazev());
        firstImgPopis.setText(selectedItem.getPrvniStranka().getPopis());
        titulImg = selectedItem.getTitulniStranka().getObrazek();
        firstImg = selectedItem.getPrvniStranka().getObrazek();
        vybranaKniha = selectedItem;
        titulImgPan.getChildren().clear();
        firstImgPan.getChildren().clear();
        titulImgPan.getChildren().add(new ImageView(new Image(new ByteArrayInputStream(selectedItem.getTitulniStranka().getObrazek()), titulImgPan.getWidth(), titulImgPan.getHeight(), true, true)));
        firstImgPan.getChildren().add(new ImageView(new Image(new ByteArrayInputStream(selectedItem.getPrvniStranka().getObrazek()), firstImgPan.getWidth(), firstImgPan.getHeight(), true, true)));

    }

    @FXML
    private void addBookHandle(ActionEvent event) {
        booksList.getSelectionModel().clearSelection();
        try {
            Insert.insertBook(
                    new Kniha(
                            null,
                            eanFild.getText(),
                            nameFild.getText(),
                            signaturaFild.getText(),
                            descriptionField.getText(),
                            autorsList.getSelectionModel().getSelectedItem(),
                            kategorieList.getSelectionModel().getSelectedItem(),
                            tiskarList.getSelectionModel().getSelectedItem(),
                            mistoVydaniList.getSelectionModel().getSelectedItem(),
                            new Obrazky(null, titulImgPopis.getText(), titulImgName.getText(), titulImg),
                            new Obrazky(null, firstImgPopis.getText(), firstImgName.getText(), firstImg),
                            new RokVydani(null, Integer.parseInt(rokVydaniStart.getText()), Integer.parseInt(rokVydaniKonec.getText()))
                    )
            );
        } catch (IOException | SQLException ex) {
            System.out.println(ex.getMessage());
        }
        this.loadBooks("");
    }

    @FXML
    private void addTiskarHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Pridat tiskare");
        dialog.setHeaderText("Pridat tiskare.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Name");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(jmeno, 1, 0);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return jmeno.getText();
                //return new Pair<>(jmeno.getText(), null);
            }
            return null;
        });
        Optional<String> result = dialog.showAndWait();
        Tiskar tiskar = new Tiskar(null, result.get());
        try {
            Insert.insertTiskar(tiskar);
            this.loadTiskare();
            //UserAuth.login(.getKey(), dialog.getResult().getValue());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    @FXML
    private void addAutorHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Pridat tiskare");
        dialog.setHeaderText("Pridat tiskare.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Name");

        TextField prijmeni = new TextField();
        prijmeni.setPromptText("prijmeni");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(jmeno, 1, 0);

        grid.add(new Label("prijmeni:"), 0, 1);
        grid.add(prijmeni, 1, 1);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(jmeno.getText(), prijmeni.getText());
            }
            return null;
        });
        Optional<Pair<String, String>> result = dialog.showAndWait();
        Autor autor = new Autor(null, result.get().getKey(), result.get().getValue());
        try {
            Insert.insertAutor(autor);
            this.loadAutors();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void addKategorieHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Pridat tiskare");
        dialog.setHeaderText("Pridat tiskare.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Name");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(jmeno, 1, 0);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return jmeno.getText();
                //return new Pair<>(jmeno.getText(), null);
            }
            return null;
        });
        Optional<String> result = dialog.showAndWait();
        Kategorie kategorie = new Kategorie(null, result.get());
        try {
            Insert.insertKategorie(kategorie);
            this.loadKategorie();
            //UserAuth.login(.getKey(), dialog.getResult().getValue());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void addTitulHandle(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            try {
                byte[] readAllBytes = Files.readAllBytes(file.toPath());
                Kniha selectedItem = booksList.getSelectionModel().getSelectedItem();
                FileInputStream image = new FileInputStream(file.getPath());

                if (selectedItem != null) {
                    selectedItem.setTitulniStranka(
                            new Obrazky(selectedItem.getTitulniStranka().getId(), firstImgPopis.getText(), firstImgName.getText(), readAllBytes)
                    );
                }
                titulImg = readAllBytes;
                titulImgPan.getChildren().clear();
                titulImgPan.getChildren().add(new ImageView(new Image(image, titulImgPan.getWidth(), titulImgPan.getHeight(), true, true)));
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    @FXML
    private void addFirstHandle(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            try {
                byte[] readAllBytes = Files.readAllBytes(file.toPath());
                Kniha selectedItem = booksList.getSelectionModel().getSelectedItem();
                FileInputStream image = new FileInputStream(file.getPath());

                if (selectedItem != null) {
                    selectedItem.setPrvniStranka(
                            new Obrazky(selectedItem.getPrvniStranka().getId(), firstImgPopis.getText(), firstImgName.getText(), readAllBytes)
                    );
                }
                firstImg = readAllBytes;
                firstImgPan.getChildren().clear();
                firstImgPan.getChildren().add(new ImageView(new Image(image, firstImgPan.getWidth(), firstImgPan.getHeight(), true, true)));
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    @FXML
    private void addMistoVydniHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Pridat misto vydani");
        dialog.setHeaderText("Pridat misto vydani.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Nazev");

        grid.add(new Label("Nazev:"), 0, 0);
        grid.add(jmeno, 1, 0);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return jmeno.getText();
                //return new Pair<>(jmeno.getText(), null);
            }
            return null;
        });
        Optional<String> result = dialog.showAndWait();
        MistoVydani kategorie = new MistoVydani(null, result.get());
        try {
            Insert.insertMistoVydani(kategorie);
            this.loadMistaVydani();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    @FXML
    private void cleanHandle(ActionEvent event) {
        booksList.getSelectionModel().clearSelection();
        nameFild.clear();
        eanFild.clear();
        signaturaFild.clear();
        descriptionField.clear();
        autorsList.getSelectionModel().clearSelection();
        kategorieList.getSelectionModel().clearSelection();
        mistoVydaniList.getSelectionModel().clearSelection();
        tiskarList.getSelectionModel().clearSelection();
        rokVydaniStart.clear();
        rokVydaniKonec.clear();
        titulImgName.clear();
        titulImgPopis.clear();
        firstImgName.clear();
        firstImgPopis.clear();
        titulImg = null;
        firstImg = null;
        titulImgPan.getChildren().clear();
        firstImgPan.getChildren().clear();
    }

    @FXML
    private void searchHandle(ActionEvent event) {
        loadBooks(findField.getText());
    }

    @FXML
    private void viewBookHandle(ActionEvent event) {
        try {
            Parent root1 = (Parent) FXMLLoader.load(getClass().getResource("/GUI/FXMLDocument.fxml"));
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initStyle(StageStyle.UNDECORATED);
            stage.setTitle(nameFild.getText());
            stage.setScene(new Scene(root1));
            stage.show();
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }
}
