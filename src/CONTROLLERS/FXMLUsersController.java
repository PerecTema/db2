/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import MODELS.Entity.Role;
import MODELS.Entity.Uzivatel;
import MODELS.GRUD.Delete;
import MODELS.GRUD.Insert;
import MODELS.GRUD.Select;
import MODELS.GRUD.Update;
import MODELS.UserAuth;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author Artem
 */
public class FXMLUsersController implements Initializable {

    @FXML
    private ListView<Uzivatel> usersList;
    @FXML
    private TextField nameFild;
    @FXML
    private TextField sNameFild;
    @FXML
    private TextField emailFild;
    @FXML
    private PasswordField pswdFild;
    @FXML
    private ComboBox<Role> roleList;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.loadUsers();
        this.loadRoles();
    }

    @FXML
    private void deleteUserHandle(ActionEvent event) {
        if (!"Admin".equals(UserAuth.getROLE())) {
            System.out.println("nemate paravo");
            //return;
        }
        try {
            Delete.uzivatelDelete(usersList.getSelectionModel().getSelectedItem());
            this.loadUsers();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void saveUserHandle(ActionEvent event) {
        try {
            Uzivatel uzivatel = usersList.getSelectionModel().getSelectedItem();
            uzivatel.setEmail(emailFild.getText());
            uzivatel.setJmenoUzivatele(nameFild.getText());
            uzivatel.setPrijmeniUzivatele(sNameFild.getText());
            uzivatel.setRole(roleList.getSelectionModel().getSelectedItem());
            Update.updateUser(
                    uzivatel, pswdFild.getText()
            );
            this.loadUsers();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadUsers() {
        try {
            usersList.setItems(Select.getUsers());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadRoles() {
        try {
            roleList.setItems(Select.getRoles());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    public void handleChusUser(MouseEvent arg0) {
        Uzivatel selectedItem = usersList.getSelectionModel().getSelectedItem();
        nameFild.setText(selectedItem.getJmenoUzivatele());
        sNameFild.setText(selectedItem.getPrijmeniUzivatele());
        emailFild.setText(selectedItem.getEmail());
        roleList.getSelectionModel().select(selectedItem.getRole());
    }

    @FXML
    private void addUserHandle(ActionEvent event) {
        try {
            Insert.insertUser(new Uzivatel(null, nameFild.getText(), sNameFild.getText(), emailFild.getText(), roleList.getSelectionModel().getSelectedItem()), pswdFild.getText());
            this.loadUsers();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
