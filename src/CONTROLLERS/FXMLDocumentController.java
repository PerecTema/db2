/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLERS;

import MODELS.Entity.Autor;
import MODELS.Entity.Kniha;
import MODELS.Entity.Motiv;
import MODELS.Entity.MotivVyzdoba;
import MODELS.Entity.Obrazky;
import MODELS.Entity.PoziceVyzdoby;
import MODELS.Entity.RokVydani;
import MODELS.Entity.Tiskar;
import MODELS.Entity.Vyzdoba;
import MODELS.GRUD.Delete;
import MODELS.GRUD.Insert;
import MODELS.GRUD.Select;
import MODELS.GRUD.Update;
import MODELS.ImageClass;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;
import javax.accessibility.AccessibleState;

/**
 * FXML Controller class
 *
 * @author Artem
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private ListView<Vyzdoba> vyzdobyList;
    @FXML
    private ListView<Motiv> themeList;
    @FXML
    private VBox imagesPanel;
    @FXML
    private ComboBox<PoziceVyzdoby> positionsList;
    @FXML
    private ListView<Vyzdoba> childrenList;
    @FXML
    private ComboBox<Vyzdoba> parentList;
    @FXML
    private TextField nazevField;
    @FXML
    private TextArea popisField;
    @FXML
    private TextField cisloStrankyField;
    @FXML
    private TextField velikostSirField;
    @FXML
    private TextField velikostVysField;
    @FXML
    private TextField rokKonecField;
    @FXML
    private TextField rokZacatekField;
    @FXML
    private ComboBox<Autor> autorsList;
    @FXML
    private ComboBox<Tiskar> tiskarList;
    @FXML
    private AnchorPane pane2;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        themeList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //FXMLBooksController.vybranaKniha = new Kniha(101, null, null, null, null, null, null, null, null, null, null, null);
        this.loadVyzdoby();
        this.loadAutors();
        this.loadPozice();
        this.loadTiskare();
        this.loadMotivy();
    }

    private void clear() {
        imagesPanel.getChildren().clear();
        nazevField.clear();
        popisField.clear();
        cisloStrankyField.clear();
        velikostSirField.clear();
        velikostVysField.clear();
        rokZacatekField.clear();
        rokKonecField.clear();
        positionsList.getSelectionModel().clearSelection();
        autorsList.getSelectionModel().clearSelection();
        themeList.getSelectionModel().clearSelection();
        tiskarList.getSelectionModel().clearSelection();
        childrenList.getSelectionModel().clearSelection();
        parentList.getSelectionModel().clearSelection();
    }

    private void loadVyzdoby() {
        try {
            vyzdobyList.setItems(Select.getVyzdoby(FXMLBooksController.vybranaKniha.getId()));
        } catch (SQLException ex) {
            Logger.getLogger("").log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    public void handleMouseClick(MouseEvent arg0) {
        this.clear();
        Vyzdoba selectedItem = vyzdobyList.getSelectionModel().getSelectedItem();
        nazevField.setText(selectedItem.getNazevVyzdoby());
        popisField.setText(selectedItem.getPopisVyzdoby());
        cisloStrankyField.setText(selectedItem.getCisloStranky().toString());
        velikostSirField.setText(selectedItem.getVelikostSirka().toString());
        velikostVysField.setText(selectedItem.getVelikostVyska().toString());
        rokZacatekField.setText(selectedItem.getRokId().getRokZacatek().toString());
        rokKonecField.setText(selectedItem.getRokId().getRokKonec().toString());
        positionsList.getSelectionModel().select(selectedItem.getPoziceId());
        autorsList.getSelectionModel().select(selectedItem.getAutorId());
        themeList.getSelectionModel().clearSelection();
        tiskarList.getSelectionModel().select(selectedItem.getTiskarId());
        try {
            parentList.setItems(Select.getVyzdoby(FXMLBooksController.vybranaKniha.getId()));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        for (Vyzdoba next : parentList.getItems()) {
            if (next.getParentId() > 0 && next.getParentId() != null && Objects.equals(next.getParentId(), selectedItem.getParentId())) {
                parentList.getSelectionModel().select(next);
            }
        }
        try {
            childrenList.setItems(Select.getPotomkyVyzdoby(selectedItem.getIdVyzdoby()));
            this.loadImages();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        ObservableList<Motiv> motivy = selectedItem.getMotivy();
        ObservableList<Motiv> items = themeList.getItems();
        items.forEach((next) -> {
            for (Iterator<Motiv> iterator = motivy.iterator(); iterator.hasNext();) {
                if (Objects.equals(next.getIdMotivu(), iterator.next().getIdMotivu())) {
                    themeList.getSelectionModel().select(next);
                }
            }
        });
    }

    @FXML
    private void addNewTiskarHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Pridat tiskare");
        dialog.setHeaderText("Pridat tiskare.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Name");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(jmeno, 1, 0);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return jmeno.getText();
                //return new Pair<>(jmeno.getText(), null);
            }
            return null;
        });
        Optional<String> result = dialog.showAndWait();
        Tiskar tiskar = new Tiskar(null, result.get());
        try {
            Insert.insertTiskar(tiskar);
            this.loadTiskare();
            //UserAuth.login(.getKey(), dialog.getResult().getValue());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    @FXML
    private void addNewAutorHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Pridat tiskare");
        dialog.setHeaderText("Pridat tiskare.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Name");

        TextField prijmeni = new TextField();
        prijmeni.setPromptText("prijmeni");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(jmeno, 1, 0);

        grid.add(new Label("prijmeni:"), 0, 1);
        grid.add(prijmeni, 1, 1);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(jmeno.getText(), prijmeni.getText());
            }
            return null;
        });
        Optional<Pair<String, String>> result = dialog.showAndWait();
        Autor autor = new Autor(null, result.get().getKey(), result.get().getValue());
        try {
            Insert.insertAutor(autor);
            this.loadAutors();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void addVyzdoba(ActionEvent event) {
        try {
            Vyzdoba vyzdoba = new Vyzdoba(
                    null,
                    nazevField.getText(),
                    popisField.getText(),
                    Integer.parseInt(cisloStrankyField.getText()),
                    Integer.parseInt(velikostSirField.getText()),
                    Integer.parseInt(velikostVysField.getText()),
                    new RokVydani(
                            null,
                            Integer.parseInt(rokZacatekField.getText()),
                            Integer.parseInt(rokKonecField.getText())
                    ),
                    null,
                    positionsList.getSelectionModel().getSelectedItem(),
                    autorsList.getSelectionModel().getSelectedItem(),
                    tiskarList.getSelectionModel().getSelectedItem(),
                    null
            );
            if (parentList.getSelectionModel().getSelectedItem() != null) {
                vyzdoba.setParentId(parentList.getSelectionModel().getSelectedItem().getIdVyzdoby());
            }
            Insert.insertVyzdoba(
                    vyzdoba,
                    FXMLBooksController.vybranaKniha.getId()
            );
            this.loadVyzdoby();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void delImage(ActionEvent event) {
    }

    @FXML
    private void addNewThemeHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Pridat motiv");
        dialog.setHeaderText("Pridat motiv.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Name");

        TextField prijmeni = new TextField();
        prijmeni.setPromptText("Description");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(jmeno, 1, 0);

        grid.add(new Label("Description:"), 0, 1);
        grid.add(prijmeni, 1, 1);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(jmeno.getText(), prijmeni.getText());
            }
            return null;
        });
        Optional<Pair<String, String>> result = dialog.showAndWait();
        Motiv motiv = new Motiv(null, result.get().getKey(), result.get().getValue());
        try {
            Insert.insertThem(motiv);
            this.loadMotivy();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadImages() throws SQLException {
        ObservableList<Obrazky> skenyVyzdoby = Select.getSkenyVyzdoby(vyzdobyList.getSelectionModel().getSelectedItem().getIdVyzdoby());
        for (int i = 0; i < skenyVyzdoby.size(); i++) {
            ImageView imageView = ImageClass.createImageView(skenyVyzdoby.get(i), (Stage) imagesPanel.getScene().getWindow());
            imagesPanel.getChildren().add(imageView);
            pane2.setPrefHeight(imageView.getFitWidth() + pane2.getPrefWidth());
        }
    }

    private void loadAutors() {
        try {
            autorsList.setItems(Select.getAutors());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    private void loadTiskare() {
        try {
            tiskarList.setItems(Select.getTiskare());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadMotivy() {
        try {
            themeList.setItems(Select.getMotivy());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private void loadPozice() {
        try {
            positionsList.setItems(Select.getPoziceVyzdoby());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void addNewPositionHandle(ActionEvent event) {
        // Create the custom dialog.
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Pridat posice");
        dialog.setHeaderText("Pridat posice.");
        ButtonType loginButtonType = new ButtonType("Pridat", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

        TextField jmeno = new TextField();
        jmeno.setPromptText("Nazev");

        grid.add(new Label("Nazev:"), 0, 0);
        grid.add(jmeno, 1, 0);

        //Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return jmeno.getText();
                //return new Pair<>(jmeno.getText(), null);
            }
            return null;
        });
        Optional<String> result = dialog.showAndWait();
        PoziceVyzdoby tiskar = new PoziceVyzdoby(null, result.get());
        try {
            Insert.insertPozice(tiskar);
            this.loadPozice();
            //UserAuth.login(.getKey(), dialog.getResult().getValue());
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }

    @FXML
    private void addNewImageHandle(ActionEvent event) throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            try {
                byte[] readAllBytes = Files.readAllBytes(file.toPath());
                Vyzdoba selectedItem = vyzdobyList.getSelectionModel().getSelectedItem();

                Insert.insertObrazekVyzdoby(selectedItem, new Obrazky(null, " ", " ", readAllBytes));
                this.loadImages();
            } catch (FileNotFoundException | SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    @FXML
    private void selectedMotiv(MouseEvent event) {
        try {
            Delete.del(MotivVyzdoba.TABLE, MotivVyzdoba.COL_VYZDOBA_ID, vyzdobyList.getSelectionModel().getSelectedItem().getIdVyzdoby());
            for (Motiv next : themeList.getSelectionModel().getSelectedItems()) {
                Insert.insertMotivVyzdoba(next, vyzdobyList.getSelectionModel().getSelectedItem());
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void editAction(ActionEvent event) {
        Vyzdoba selectedItem = vyzdobyList.getSelectionModel().getSelectedItem();
        selectedItem.setNazevVyzdoby(nazevField.getText());
        selectedItem.setPopisVyzdoby(popisField.getText());
        selectedItem.setCisloStranky(Integer.parseInt(cisloStrankyField.getText()));
        selectedItem.setVelikostSirka(Integer.parseInt(velikostSirField.getText()));
        selectedItem.setVelikostVyska(Integer.parseInt(velikostVysField.getText()));
        selectedItem.getRokId().setRokZacatek(Integer.parseInt(rokZacatekField.getText()));
        selectedItem.getRokId().setRokKonec(Integer.parseInt(rokKonecField.getText()));
        selectedItem.setTiskarId(tiskarList.getSelectionModel().getSelectedItem());
        selectedItem.setAutorId(autorsList.getSelectionModel().getSelectedItem());
        selectedItem.setPoziceId(positionsList.getSelectionModel().getSelectedItem());
        selectedItem.setParentId(parentList.getSelectionModel().getSelectedItem().getIdVyzdoby());
        try {
            Update.updateVyzdoba(selectedItem);
            this.loadVyzdoby();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
