/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import MODELS.OracleConnector;
import MODELS.UserAuth;
import java.util.Optional;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 *
 * @author Artem
 */
public class DB2 extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        if (!OracleConnector.isConect()) {
            // Create the custom dialog.
            Dialog<Pair<String, String>> dialog = new Dialog<>();
            dialog.setTitle("Login");
            dialog.setHeaderText("Please, login.");

// Set the icon (must be included in the project).
            dialog.setGraphic(new ImageView(new Image("https://cdn2.iconfinder.com/data/icons/large-svg-icons-part-2/512/registration_registration2-512.png", 70, 70, true, true)));
// Set the button types.
            ButtonType loginButtonType = new ButtonType("Login", ButtonData.OK_DONE);
            dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

// Create the username and password labels and fields.
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new javafx.geometry.Insets(20, 150, 10, 10));

            TextField username = new TextField();
            username.setPromptText("Username");
            PasswordField password = new PasswordField();
            password.setPromptText("Password");

            grid.add(new Label("Username:"), 0, 0);
            grid.add(username, 1, 0);
            grid.add(new Label("Password:"), 0, 1);
            grid.add(password, 1, 1);

// Enable/Disable login button depending on whether a username was entered.
            Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
            loginButton.setDisable(true);

// Do some validation (using the Java 8 lambda syntax).
            username.textProperty().addListener((observable, oldValue, newValue) -> {
                loginButton.setDisable(newValue.trim().isEmpty());
            });

            dialog.getDialogPane().setContent(grid);

// Request focus on the username field by default.
            Platform.runLater(() -> username.requestFocus());

// Convert the result to a username-password-pair when the login button is clicked.
            dialog.setResultConverter(dialogButton -> {
                if (dialogButton == loginButtonType) {
                    return new Pair<>(username.getText(), password.getText());
                }
                return null;
            });

            do {
                Optional<Pair<String, String>> result = dialog.showAndWait();
                if (result.isPresent() || !UserAuth.isAuth()) {
                    OracleConnector.conect("C##ST47119", "ST47119");
                    try {
                        UserAuth.login(dialog.getResult().getKey(), dialog.getResult().getValue());
                    } catch (SecurityException e) {
                        System.out.println(e.getMessage());
                    }
                } else {
                    System.exit(1);
                }

            } while (!OracleConnector.isConect() || !UserAuth.isAuth());

        }
        OracleConnector.getConnection().setAutoCommit(false);
        //Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLDocument.fxml")));
        Scene scene = new Scene(FXMLLoader.load(getClass().getResource("FXMLBooks.fxml")));

        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
